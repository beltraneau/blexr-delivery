package blexr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RealMoneyPage {
    WebDriver driver;
    String [] xpatharray = {"//*[@id=\"vegas-body\"]/main/div[3]/section[6]/div/div[2]/div[3]/div[2]/p/a[4]","//*[@id=\"vegas-body\"]/main/div[3]/section[6]/div/div[2]/div[5]/div[2]/p/a","//*[@id=\"vegas-body\"]/main/div[3]/section[6]/div/div[2]/div[3]/div[2]/p/a[2]","//*[@id=\"vegas-body\"]/main/div[3]/section[3]/div/div[1]/p[1]/a","//*[@id=\"vegas-body\"]/main/div[3]/section[6]/div/div[2]/div[4]/div[2]/p[1]/a[2]"};
    String [] hrefarray = {"/live-dealer/","/deposit-method/","/table-games/","/reviews/","/no-deposit-bonuses/"};
    String baseURL = "https://www.vegasslotsonline.com";


    public RealMoneyPage(WebDriver driver) {
        this.driver=driver;
    }


    public void verifyLinkCoincidence (String linkurl,String hrefRef) {

            if (linkurl.contentEquals(hrefRef)){
                System.out.println("Valid link: "+ linkurl);
            }
            else
                System.out.println("Invalid link "+linkurl);
    }

    public void LinkComparison(){
        for(int i =0;i<xpatharray.length;i++){
            WebElement element = driver.findElement(By.xpath(xpatharray[i]));
            String url = element.getAttribute("href");
            verifyLinkCoincidence(url,baseURL+hrefarray[i]);
        }
    }
}
