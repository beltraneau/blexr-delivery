package blexr.tests;

import blexr.pages.HomePage;
import blexr.pages.RealMoneyPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class BlexrTests {
    WebDriver driver;
    String baseURL = "https://www.vegasslotsonline.com/";
    String baseURL2 = "https://www.vegasslotsonline.com/real-money/";
    String chromepath =System.getProperty("user.dir") + "\\drivers\\chromedriver83.exe";
    HomePage homePage;
    RealMoneyPage realMoneyPage;


    @BeforeTest
    public void setUp(){
        System.setProperty("webdriver.chrome.driver",chromepath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseURL);

    }

    @AfterTest
    public void tearDown(){
        driver.quit();
    }
/*
    @Test(priority=0)
    public void FooterLinkVerification(){
        homePage = new HomePage(driver);
        homePage.HomePageLinkVerification();
    }
*/
    @Test (priority=1)
    public void registerAnUser(){
        realMoneyPage = new RealMoneyPage(driver);
        driver.get(baseURL2);
        realMoneyPage.LinkComparison();
    }

}
