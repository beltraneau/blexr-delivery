package blexr.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;

public class HomePage {
    WebDriver driver;

    By taglinks = By.tagName("a");

    public HomePage (WebDriver driver){
        this.driver=driver;
    }
    public void verifyLinkConnection (String linkurl) {
        try {
            URL url = new URL(linkurl);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            http.setRequestMethod("GET");
            http.connect();
            int response= http.getResponseCode();
            if (response == 200){
                System.out.println("Valid link: "+ linkurl);
            }
            else
                System.out.println("Invalid link"+linkurl);
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

        public void HomePageLinkVerification(){
        List <WebElement> links = driver.findElements(taglinks);
        int code = -1;
        for(int i =0;i<links.size();i++){
            WebElement element = links.get(i);
            String url = element.getAttribute("href");
            verifyLinkConnection(url);
        }
    }
}
